CC = gcc -Wall
OPFLAGS = -O2 -march=native
LDFLAGS = -lOpenCL -lm

all: example clean

example: ocl_create_programs.o example.o
	$(CC) $? $(LDFLAGS) -o $@

example.o: example.c
	$(CC) $(OPFLAGS) $? -c

ocl_create_programs.o: ocl_create_programs.c
	$(CC) $(OPFLAGS) $? -c

clean:
	rm -f ocl_create_programs.o example.o
