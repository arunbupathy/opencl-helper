
__kernel void matadd(__global int * matrixa, __global int * matrixb, __global int * matrixc)
{
    int i = get_global_id(0);
    
    matrixc[i] = matrixa[i] + matrixb[i];
}
