# opencl-helper

### A helper function that abstracts away some of the boilerplate code that is required to program your GPU(s) using OpenCL.

You will simply have to provide the helper function a list of OpenCL source files, and it will: (i) find all OpenCL devices exposed by your system, (ii) create a separate context for every device found, and (iii) compile the given OpenCL source files for each of these devices, and returns the list of devices, contexts, and program objects.

### Function definition:

`cl_program * ocl_create_programs(char * cl_source[], cl_uint num_sources, cl_uint * total_devices, cl_context ** contexts, cl_device_id ** devices)`

The function takes the following inputs:

*cl_source[]* is the list of OpenCL source files that are to be compiled.

*num_sources* is the number of OpenCL source files in the list.

The following objects are its outputs:

_(*total_devices)_ is the number of OpenCL devices found in the system.

_(*contexts)_ is a list of contexts, one for each device found.

_(*devices)_ is the list of devices found in the system.

The return value of the function is a list of program objects that have been compiled from the given source files.

There is one program object per device/context, and is only valid for the device/context for which it was compiled. More advanced functionality like multiple devices in a single context is outside the scope of this helper.

See the given example program for usage.
