
# define CL_TARGET_OPENCL_VERSION 120

# include <stdio.h>
# include <math.h>
# include <stdlib.h>
# include "ocl_create_programs.h"

# define MSIZE 1024

int main(void)
{
    // arrays to hold the input and output
    int * matrixa = (int*) malloc(MSIZE * MSIZE * sizeof(int));
    int * matrixb = (int*) malloc(MSIZE * MSIZE * sizeof(int));
    int * matrixc = (int*) malloc(MSIZE * MSIZE * sizeof(int));
    
    // initialize the input matrices
    for(int i = 0; i < MSIZE * MSIZE; ++i)
    {
        matrixa[i] = i;
        matrixb[i] = 2;
    }
    
    // create a list of the opencl kernel source files; in this example we have two source files
    char * mysources[2] = {(char *) "matadd.cl", (char *) "matmul.cl"};
    
    cl_uint num_cl_dev; // this is to hold the total number of opencl devices found in the system
    cl_device_id * devices; // to hold the list of devices found by the wrapper function below
    cl_context * contexts; // to hold the list of contexts created by the wrapper function below

    // this function takes all the source files that we give it and creates program objects,
    // i.e, an "object" containing the compiled machine code, for every device that it has found.
    // each program in the "programs" list contains the object code for all the given opencl sources
    // but is only valid for the device for which it was compiled, i.e., programs[0] is for device[0]
    // it creates a separate context for each device, irrespective of the platform the device is from.
    cl_program * programs = ocl_create_programs(mysources, 2, &num_cl_dev, &contexts, &devices);
    // the second argument is the number of source files that we have.
    
    cl_int cl_status; // this is to hold the success/failure status returned by most opencl api calls
    // opencl api function calls begin with a "cl" prefix
    
    int mycldev = 0; // choose the first opencl device found in the system and create a command submission queue
    cl_command_queue myqueue = clCreateCommandQueue(contexts[mycldev], devices[mycldev], 0, NULL);
    
    // create buffers on the device; the first two also copy the data to the device from the provided pointer
    cl_mem d_matrixa = clCreateBuffer(contexts[mycldev], CL_MEM_READ_ONLY | CL_MEM_HOST_WRITE_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(int) * MSIZE * MSIZE, matrixa, &cl_status);
    cl_mem d_matrixb = clCreateBuffer(contexts[mycldev], CL_MEM_READ_ONLY | CL_MEM_HOST_WRITE_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(int) * MSIZE * MSIZE, matrixb, &cl_status);
    cl_mem d_matrixc = clCreateBuffer(contexts[mycldev], CL_MEM_WRITE_ONLY | CL_MEM_HOST_READ_ONLY, sizeof(int) * MSIZE * MSIZE, NULL, &cl_status);
    
    // create matrix add kernel from the programs object, and call it mykernel1 on the host side
    cl_kernel mykernel1 = clCreateKernel(programs[mycldev], "matadd", &cl_status);
    
    // set the kernel arguments for the matadd() kernel
    cl_status = clSetKernelArg(mykernel1, 0, sizeof(cl_mem), &d_matrixa);
    cl_status = clSetKernelArg(mykernel1, 1, sizeof(cl_mem), &d_matrixb);
    cl_status = clSetKernelArg(mykernel1, 2, sizeof(cl_mem), &d_matrixc);
    
    // define the total number of work items (WorkSize), and the size of each workgroup (GroupSize)
    size_t WorkSize = MSIZE * MSIZE; // will need padding if worksize is not a multiple of groupsize
    size_t GroupSize = 64;
    
    // enqueue the kernel
    cl_status = clEnqueueNDRangeKernel(myqueue, mykernel1, 1, NULL, &WorkSize, &GroupSize, 0, NULL, NULL);
    
    // force the device to finish the commands in the queue (especially if it can do out-of-order queues)
    cl_status = clFinish(myqueue);
    
    // read back the result
    cl_status = clEnqueueReadBuffer(myqueue, d_matrixc, CL_TRUE, 0, sizeof(int) * MSIZE * MSIZE, matrixc, 0, NULL, NULL);
    
    // print the first few elements of the output
    for(int i = 0; i < MSIZE; i++)
        printf("%d\n", matrixc[i]);
    
    // now create matrix multiply kernel from the programs object, and call it mykernel2
    cl_kernel mykernel2 = clCreateKernel(programs[mycldev], "matmul", &cl_status);
    
    // rinse and repeat
    cl_status = clSetKernelArg(mykernel2, 0, sizeof(cl_mem), &d_matrixa);
    cl_status = clSetKernelArg(mykernel2, 1, sizeof(cl_mem), &d_matrixb);
    cl_status = clSetKernelArg(mykernel2, 2, sizeof(cl_mem), &d_matrixc);
    
    cl_status = clEnqueueNDRangeKernel(myqueue, mykernel2, 1, NULL, &WorkSize, &GroupSize, 0, NULL, NULL);
    
    cl_status = clFinish(myqueue);
    
    cl_status = clEnqueueReadBuffer(myqueue, d_matrixc, CL_TRUE, 0, sizeof(int) * MSIZE * MSIZE, matrixc, 0, NULL, NULL);
    
    for(int i = 0; i < MSIZE; i++)
        printf("%d\n", matrixc[i]);
    
    // destroy the kernel objects 
    clReleaseKernel(mykernel1);
    clReleaseKernel(mykernel2);
    
    // destroy the memory objects
    clReleaseMemObject(d_matrixa);
    clReleaseMemObject(d_matrixb);
    clReleaseMemObject(d_matrixc);
    
    // destroy the queue
    clReleaseCommandQueue(myqueue);

    // destroy the programs and the contexts
    // the devices needn't be destroyed, because we weren't the ones who created them!
    for(cl_uint icldev = 0; icldev < num_cl_dev; ++icldev)
    {
        clReleaseProgram(programs[icldev]);
        clReleaseContext(contexts[icldev]);
    }
    
    // now free the programs, devices and contexts list (arrays)
    free(programs);
    free(devices);
    free(contexts);
    
    return 0;
}
